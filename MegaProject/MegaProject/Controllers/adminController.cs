﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;
using System.Data.Entity;

namespace MegaProject.Controllers
{
    public class adminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: admin
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                return RedirectToAction("dashboard", "Home");
            }
                return RedirectToAction("Login", "Account");
        }
        public ActionResult show_contact()
        {
            return View();
        }
        [HttpPost]
        public void PassValStatus(bool val, int id)
        {
            var obj = db.Store_request.Find(id);
            obj.Store_request_status = val;
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
            //var js = new JavaScriptSerializer();
            //bool value = JsonConvert.DeserializeObject<bool>(form["val"]);
        }
        public ActionResult all_become_member()
        {
            return View();
        }

    }
}