﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;
using System.Web.Script.Serialization;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Drawing;
using Watermark;
using System.Windows.Forms;

namespace MegaProject.Controllers
{
    public class StoresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Stores
        public ActionResult Index()
        {
            return View();
        }
    

        public ActionResult All_Stores()
        {
            var All_stores = db.Stores.Where(x => x.Status).ToList();
            return View(All_stores);
        }

        // GET: Stores/Details/5
        public  ActionResult Index2()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Check_Number_Exist(System.Web.Mvc.FormCollection form)
        {
            try
            {
                var js = new JavaScriptSerializer();
                string StoreNumber = JsonConvert.DeserializeObject<string>(form["StoreNumber"]);
               
                if (StoreNumber != null)
                {
                    var Storenum = db.Stores.Where(m => m.StoreNumber == StoreNumber).SingleOrDefault();
                    if (Storenum!=null) {
                        return Json("False Storenum", JsonRequestBehavior.AllowGet);
                    }
                   
                   
                }
              
            }
            catch (Exception ex)
            {
               
                
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Check_OwnerNumber_Exist(System.Web.Mvc.FormCollection form)
        {
            try
            {
                var js = new JavaScriptSerializer();
                
                string StoreOwnerNumber = JsonConvert.DeserializeObject<string>(form["StoreOwnerNumber"]);
              
                if (StoreOwnerNumber != null)
                {
                    var StoreOwnerNum = db.Stores.Where(m => m.StoreOwnerNumber == StoreOwnerNumber).SingleOrDefault();
                    if (StoreOwnerNum != null)
                    {
                        return Json("False StoreOwnerNum", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {


            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int? id)
        {
            Count_model count_model = new Count_model();

            
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return HttpNotFound();
            }
            string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            string date1 = DateTime.Now.ToShortDateString();
            var check_ip = db.Counts_model.Where(m => m.visiter_ip== ipAddress && m.StoreId == id );
            
            if(check_ip != null)
            {

                var check_id = check_ip.Where(n => n.count_date == date1).SingleOrDefault();
                if(check_id != null)
                { 
                    check_id.count++;
                    db.Entry(check_id).State = EntityState.Modified;
                    db.SaveChanges();
              }
                else
                {
                    int countt = 1;
                    count_model.count = countt;
                    count_model.visiter_ip = ipAddress;
                    var date = DateTime.Now.ToString("dd MMMM yyyy");
                    count_model.count_date = DateTime.Now.ToShortDateString();

                    count_model.StoreId = id ?? default(int);
                    db.Counts_model.Add(count_model);
                    db.SaveChanges();
                }

            }
            else
            {
                    

                int countt = 1;
                count_model.count = countt;
                count_model.visiter_ip = ipAddress;
               
                count_model.count_date = DateTime.Now.ToShortDateString();
                count_model.StoreId = id ?? default(int);
                db.Counts_model.Add(count_model);
                db.SaveChanges();
            }





          
            return View(store);
        }
        [Authorize]
        public ActionResult store_dashboard ()
        {
            string idd = Convert.ToString(Session["UserId"]);
            if (idd == "")
            {
                return RedirectToAction("Index","Home");
            }
           
            var store = db.Stores.Where(m => m.Id == idd).SingleOrDefault();
            var store_in_count = db.Counts_model.Where(m=>m.StoreId==store.StoreId).ToList() ;
            
            return View(store_in_count);
        }


        // GET: Stores/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_Data(int StoreId)
        {
            var obj = db.Stores.Find(StoreId);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        // POST: Stores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create( Store store)
        {
           
                
                return RedirectToAction("Index");
           

            
        }

        // GET: Stores/Edit/5
        public ActionResult Edit_profile (int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return HttpNotFound();
            }
            return View(store);
        }
 
        // POST: Stores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit_profile(Store store, ApplicationUser appuser)
        {
            if (ModelState.IsValid)
            {

                Store store_update = db.Stores.Find(store.StoreId);
                store_update.StoreName = store.StoreName;
                store_update.StoreOwnerName = store.StoreOwnerName;
                store_update.StoreNumber = store.StoreNumber;
                store_update.StoreOwnerNumber = store.StoreOwnerNumber;
                store_update.StoreAddress = store.StoreAddress;
                //appuser.Email = "";
                //var result = await UserManager.UpdateAsync(appuser , )  
                db.Entry(store_update).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect("Edit_profile?id=" + store.StoreId);
            }
            return View(store);
        }


        // GET: Stores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store store = db.Stores.Find(id);
            if (store == null)
            {
                return HttpNotFound();
            }
            return View(store);
        }

        [HttpPost]
        public JsonResult Insert(System.Web.Mvc.FormCollection form)
        {
            int product_id = 0;
            try
            {
                var js = new JavaScriptSerializer();
                Store store = js.Deserialize<Store>(form["Store"]);
                store.Id = Session["StoreUserId"].ToString();
                if (Session["ImageS"] != null)
                {
                    store.StoreLogo = Session["ImageS"].ToString();

                }
                else {
                    store.StoreLogo = "no.png";

                }


              
                store.user_name = Session["Usern"].ToString();

                store.password = Session["Userpas"].ToString(); 
                //StoreType StoreType= JsonConvert.DeserializeObject<StoreType>(form["StoreType"]);
                store.StoreDate = DateTime.Now;
                store.Status = true;
                db.Stores.Add(store);
                db.SaveChanges();
                product_id = db.Stores.Max(m => m.StoreId);

            }
            catch (Exception ex)
            {

            }

            return Json(product_id, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult StoreImage(System.Web.Mvc.FormCollection form)
        {
            var js = new JavaScriptSerializer();
            try
            {
                var ProductImage = Request.Files[0];
                if (ProductImage != null)
                {
                    string filename = Guid.NewGuid() + Path.GetExtension(ProductImage.FileName);
                    ProductImage.SaveAs(Path.Combine(Server.MapPath("~/Content/Store/Images"), filename));
                    return new JsonResult { Data = new { filename = filename } };
                }
            }
            catch (Exception ex)
            { }
            return new JsonResult { Data = new { filename = string.Empty } };
        }

        [HttpPost]
        public ContentResult Upload()
        {
            string path = Server.MapPath("~/Content/Images/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            foreach (string key in Request.Files)
            {
               
                HttpPostedFileBase postedFile = Request.Files[key];
                postedFile.SaveAs(path + postedFile.FileName);
                Session["ImageS"] =  postedFile.FileName;



                //using (Image image = Image.FromStream(postedFile.InputStream, true, false))
                //{
                //    string name = Path.GetFileNameWithoutExtension(postedFile.FileName);
                //    var ext = Path.GetExtension(postedFile.FileName);
                //    string myfile = name + ext;
                //    var saveImagePath = Path.Combine(Server.MapPath("~/Content/Images/"), myfile);
                //    Image watermarkImage = Image.FromFile(Server.MapPath("/Content/Images/watermarklogo.png"));
                //    Watermarker objWatermarker = new Watermarker(image);
                //    for (int i = 0; i < image.Height; i++)
                //    {
                //        for (int j = 0; j < image.Width; j++)
                //        {

                //            // Set the properties for the logo
                //            objWatermarker.Position = WatermarkPosition.Absolute;
                //            objWatermarker.PositionX = j;
                //            objWatermarker.PositionY = i;
                //            objWatermarker.Margin = new Padding(20);
                //            objWatermarker.Opacity = 0.5f;
                //            objWatermarker.TransparentColor = Color.White;
                //            objWatermarker.ScaleRatio = 3;
                //            // Draw the logo
                //            objWatermarker.DrawImage(watermarkImage);
                //            //Draw the Text
                //            //objWatermarker.DrawText("WaterMarkDemo")

                //            j = j + 400;// watermark image width 
                //        }
                //        i = i + 120;//
                //    }
                //    objWatermarker.Image.Save(saveImagePath);
                //}
            }

            return Content("Success");
        }

        [HttpPost]
        public JsonResult AssignImagetoStore(System.Web.Mvc.FormCollection form)
        {
            try
            {
                int StoreId = Convert.ToInt32(form["StoreId"].ToString());
                string type = form["type"].ToString();
                var product = db.Stores.Find(StoreId);
                if (product != null)
                {

                    product.StoreLogo = form["imageName"].ToString();

                    db.Entry(product).State = EntityState.Modified;
                    db.SaveChanges();
                    var ret = Json(true, JsonRequestBehavior.AllowGet);
                    return ret;
                }


            }
            catch (Exception ex)
            {
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        // POST: Stores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Store store = db.Stores.Find(id);
            db.Stores.Remove(store);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Update_Store(System.Web.Mvc.FormCollection form)
        {
            var js = new JavaScriptSerializer();
            Store store = js.Deserialize<Store>(form["Store"]);
            Store store2 = db.Stores.Find(store.StoreId);

            if (Session["ImageS"] != null)
            {
                store2.StoreLogo = Session["ImageS"].ToString();
            }  
         
         
            store2.StoreAddress = store.StoreAddress;
            store2.StoreName = store.StoreName;
            store2.StoreNumber = store.StoreNumber;
            store2.StoreOwnerNumber = store.StoreOwnerNumber;
            store2.Discription = store.Discription;
            store2.Status = store.Status;

            db.Entry(store2).State = EntityState.Modified;
            db.SaveChanges();
           
        }

        public JsonResult GetStore()
        {
            var Stores = db.Stores.ToList();

            return Json(Stores, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
