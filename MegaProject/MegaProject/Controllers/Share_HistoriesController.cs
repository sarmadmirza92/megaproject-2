﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;

namespace MegaProject.Controllers
{
    public class Share_HistoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Share_Histories
        public ActionResult Index()
        {
            var share_Histories = db.Share_Histories.Include(s => s.Share);
            return View(share_Histories.ToList());
        }

        // GET: Share_Histories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_History share_History = db.Share_Histories.Find(id);
            if (share_History == null)
            {
                return HttpNotFound();
            }
            return View(share_History);
        }

        // GET: Share_Histories/Create
        public ActionResult Create()
        {
            ViewBag.Share_id = new SelectList(db.Shares, "Share_id", "Share_Type");
            return View();
        }

        // POST: Share_Histories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Share_History_id,Share_id,Share_History_Amount,Share_History_Date")] Share_History share_History)
        {
            if (ModelState.IsValid)
            {
                db.Share_Histories.Add(share_History);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

           
            return View(share_History);
        }

        // GET: Share_Histories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_History share_History = db.Share_Histories.Find(id);
            if (share_History == null)
            {
                return HttpNotFound();
            }
           
            return View(share_History);
        }

        // POST: Share_Histories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Share_History share_History)
        {
            if (ModelState.IsValid)
            {
                Share_History share_History_update = db.Share_Histories.Find(share_History.Share_History_id);
                share_History_update.Share_id = share_History.Share_id;
                
                share_History_update.Share_History_Amount = share_History.Share_History_Amount;
                share_History_update.Share_History_Date = share_History.Share_History_Date;
                //appuser.Email = "";
                //var result = await UserManager.UpdateAsync(appuser , )  
                db.Entry(share_History_update).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(share_History);
        }

        // GET: Share_Histories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_History share_History = db.Share_Histories.Find(id);
            if (share_History == null)
            {
                return HttpNotFound();
            }
            return View(share_History);
        }

        // POST: Share_Histories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Share_History share_History = db.Share_Histories.Find(id);
            db.Share_Histories.Remove(share_History);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public JsonResult GetShare()
        {
            var share_list = db.Share_Histories.ToList();
            return Json(share_list, JsonRequestBehavior.AllowGet);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
