﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;

namespace MegaProject.Controllers
{
    public class Store_requestController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Store_request
        public ActionResult Index()
        {
            return View(db.Store_request.ToList());
        }

        // GET: Store_request/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store_request store_request = db.Store_request.Find(id);
            if (store_request == null)
            {
                return HttpNotFound();
            }
            return View(store_request);
        }

        // GET: Store_request/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Store_request/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create( Store_request store_request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    store_request.Store_request_status = false;
                    store_request.Store_request_date = DateTime.Now;
                    db.Store_request.Add(store_request);

                    db.SaveChanges();

                    Session["Message"] = "success";
                }
                catch(Exception ex)
                {
                    Session["Message"] = "Error";
                }
                return RedirectToAction("Index" , "Home");
            }
     
            return View();
        }

        // GET: Store_request/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store_request store_request = db.Store_request.Find(id);
            if (store_request == null)
            {
                return HttpNotFound();
            }
            return View(store_request);
        }

        // POST: Store_request/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Store_request_id,Store_request_name,Store_request_owner_name,Store_request_type,Store_request_discription,Store_request_address,Store_request_phone_no")] Store_request store_request)
        {
            if (ModelState.IsValid)
            {
                db.Entry(store_request).State = EntityState.Modified;
                db.SaveChanges();
                
                return RedirectToAction("Index");
              
            }
            return View();
        }

        // GET: Store_request/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Store_request store_request = db.Store_request.Find(id);
            if (store_request == null)
            {
                return HttpNotFound();
            }
            return View(store_request);
        }

        // POST: Store_request/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Store_request store_request = db.Store_request.Find(id);
            db.Store_request.Remove(store_request);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
