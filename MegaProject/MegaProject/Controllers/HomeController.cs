﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;
using PagedList;

namespace MegaProject.Controllers
{
    public class HomeController : Controller
    {
        private const int defaultPageSize = 15;
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult dashboard()
        {
            return View();
        }
        //public ActionResult shops()
        //{
        //    return View();
        //}
        public ActionResult shops( int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value : 1;
            IList<Store> store_list = db.Stores.Where(s=> s.Status).ToList();
          //  store_list = store_list.ToPagedList(currentPageIndex, defaultPageSize);
            PagedList<Store> model = new PagedList<Store>(store_list, currentPageIndex, defaultPageSize);

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Privacy_policy()
        {
            return View();
        }
        public ActionResult terms_condition()
        {
            return View();
        }
        public ActionResult faq_s()
        {
            return View();
        }

        public ActionResult Contact_us()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contact_us( Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.contacts.Add(contact);
                db.SaveChanges();
                TempData["message"] = "Your Message Sent Successfully";

                return RedirectToAction("Contact_us");
            }

            return View(contact);
        }
       
  

    }
}