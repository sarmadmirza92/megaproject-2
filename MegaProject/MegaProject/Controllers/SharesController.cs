﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;

namespace MegaProject.Controllers
{
    public class SharesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Shares
        public ActionResult Index()
        {
            return View(db.Shares.ToList());
        }

        // GET: Shares/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = db.Shares.Find(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            return View(share);
        }

        // GET: Shares/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Share_id,Share_Percentage,/*Share_Type*/,Share_Amount,Share_Code,Share_Date")] Share share)
        {
            if (ModelState.IsValid)
            {
                db.Shares.Add(share);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(share);
        }

        // GET: Shares/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = db.Shares.Find(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            return View(share);
        }

        // POST: Shares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Share_id,Share_Percentage,Share_Type,Share_Amount,Share_Code,Share_Date")] Share share)
        //{
        //    if (ModelState.IsValid)
        //    {
               
        //        db.Entry(share).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(share);
        //}
        public ActionResult Edit(Share share)
        {
            if (ModelState.IsValid)
            {

                Share share_update = db.Shares.Find(share.Share_id);
                share_update.Share_Percentage= share.Share_Percentage;
               // share_update.Share_Type = share.Share_Type;
                share_update.Share_Amount = share.Share_Amount;
                share_update.Share_Code = share.Share_Code;
                share_update.Share_Date = share.Share_Date;
                //appuser.Email = "";
                //var result = await UserManager.UpdateAsync(appuser , )  
                db.Entry(share_update).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(share);
        }

        // GET: Shares/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share share = db.Shares.Find(id);
            if (share == null)
            {
                return HttpNotFound();
            }
            return View(share);
        }

        // POST: Shares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Share share = db.Shares.Find(id);
            db.Shares.Remove(share);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
