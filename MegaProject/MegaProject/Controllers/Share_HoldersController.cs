﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;

namespace MegaProject.Controllers
{
    public class Share_HoldersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Share_Holders
        public ActionResult Index()
        {
            var share_Holders = db.Share_Holders.Include(s => s.ApplicationUser).Include(s => s.Share_History);
            return View(share_Holders.ToList());
        }

        // GET: Share_Holders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_Holder share_Holder = db.Share_Holders.Find(id);
            if (share_Holder == null)
            {
                return HttpNotFound();
            }
            return View(share_Holder);
        }

        // GET: Share_Holders/Create
        public ActionResult Create()
        {
            
         //   ViewBag.Share_History_id = new SelectList(db.Share_Histories, "Share_History_id", "Share_History_id");
            return View();
        }

        // POST: Share_Holders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Share_Holder_id,Id,Share_History_id,Share_Holder_Name,Share_Holder_UniqueId,Share_Holder_Password,Share_Holder_Date")] Share_Holder share_Holder)
        {
            if (ModelState.IsValid)
            {
                //string userId = "";
                //userId = Session["usrnamw"].ToString(); 
                db.Share_Holders.Add(share_Holder);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

          
            ViewBag.Share_History_id = new SelectList(db.Share_Histories, "Share_History_id", "Share_History_id", share_Holder.Share_History_id);
            return View(share_Holder);
        }

        // GET: Share_Holders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_Holder share_Holder = db.Share_Holders.Find(id);
            if (share_Holder == null)
            {
                return HttpNotFound();
            }
         
            ViewBag.Share_History_id = new SelectList(db.Share_Histories, "Share_History_id", "Share_History_id", share_Holder.Share_History_id);
            return View(share_Holder);
        }

        // POST: Share_Holders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Share_Holder share_Holder)
        {
            if (ModelState.IsValid)
            {
                Share_Holder share_holder_update = db.Share_Holders.Find(share_Holder.Share_Holder_id);
                share_holder_update.Share_History_id = share_Holder.Share_History_id;
                share_holder_update.Share_Holder_Name = share_Holder.Share_Holder_Name;
                share_holder_update.Share_Holder_UniqueId = share_Holder.Share_Holder_UniqueId;
                share_holder_update.Share_Holder_Password = share_Holder.Share_Holder_Password;
                share_holder_update.Share_Holder_Date = share_Holder.Share_Holder_Date;

                db.Entry(share_holder_update).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(share_Holder);
        }

        // GET: Share_Holders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Share_Holder share_Holder = db.Share_Holders.Find(id);
            if (share_Holder == null)
            {
                return HttpNotFound();
            }
            return View(share_Holder);
        }

        // POST: Share_Holders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Share_Holder share_Holder = db.Share_Holders.Find(id);
            db.Share_Holders.Remove(share_Holder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Shareinfo()
        {
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
