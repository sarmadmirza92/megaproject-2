﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MegaProject.Models;

namespace MegaProject.Controllers
{
    public class PlazasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Plazas
        public ActionResult Index()
        {
            var plazas = db.Plazas.Include(p => p.Market);
            return View(plazas.ToList());
        }

        // GET: Plazas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plaza plaza = db.Plazas.Find(id);
            if (plaza == null)
            {
                return HttpNotFound();
            }
            return View(plaza);
        }

        // GET: Plazas/Create
        public ActionResult Create()
        {
            ViewBag.Market_id = new SelectList(db.Markets, "Market_id", "Market_Name");
            return View();
        }

        // POST: Plazas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Plaza_id,Market_id,Plaza_Name,Plaza_Status")] Plaza plaza)
        {
            if (ModelState.IsValid)
            {
                plaza.Plaza_Status = true;
                db.Plazas.Add(plaza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Market_id = new SelectList(db.Markets, "Market_id", "Market_Name", plaza.Market_id);
            return View(plaza);
        }

        // GET: Plazas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plaza plaza = db.Plazas.Find(id);
            if (plaza == null)
            {
                return HttpNotFound();
            }
            ViewBag.Market_id = new SelectList(db.Markets, "Market_id", "Market_Name", plaza.Market_id);
            return View(plaza);
        }

        // POST: Plazas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Plaza_id,Market_id,Plaza_Name,Plaza_Status")] Plaza plaza)
        {
            if (ModelState.IsValid)
            {
                plaza.Plaza_Status = true;
                db.Entry(plaza).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Market_id = new SelectList(db.Markets, "Market_id", "Market_Name", plaza.Market_id);
            return View(plaza);
        }

        // GET: Plazas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Plaza plaza = db.Plazas.Find(id);
            if (plaza == null)
            {
                return HttpNotFound();
            }
            return View(plaza);
        }

        // POST: Plazas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Plaza plaza = db.Plazas.Find(id);
            plaza.Plaza_Status = false;
            db.Entry(plaza).State = EntityState.Modified;
           // db.Plazas.Remove(plaza);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
