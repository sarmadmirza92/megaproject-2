﻿App.controller("ExpenseCtrl", function ($scope, $http) {

    $scope.list = [];
    $scope.Grid = [];

    $scope.Expense_Child_List = [];
    $scope.Expense_Parent_Data = [];

    $scope.selectedAll = false;
    $scope.AllowAddNewRow = true;
    $scope.AllowSubmit = false;

    $scope.Expense_Parent_Obj = { Expense_Parent_id: 0, Expense_Total_Amount: 0,  };



    $scope.AddNew = function () {
        $scope.AllowAddNewRow = true;
        angular.forEach($scope.Expense_Child_List, function (obj) {
            if (obj.Expense_Child_Description == "") {
                $scope.AllowAddNewRow = false;
                return;
            }
        });
    };

    if ($scope.AllowAddNewRow)
    {
        $scope.Expense_Child_Listt.push({
            'Expense_Child_id' :0,
            'Expense_Parent_id' : 0,
            'Expense_Child_Amount' :0,
            'Expense_Child_Description': "",
        });
    }

    $scope.SaveExpense = function () {
        angular.forEach($scope.Expense_Child_List, function (obj) {
            obj.Expense_Parent = null;
        });
    }
       
    $scope.CheckAll = function () {
        if (!$scope.selectedAll) {
            $scope.selectedAll = false;
        } else {
            $scope.selectedAll = true;
        }
        angular.forEach($scope.Expense_Child_List, function (obj) {
            obj.selected = $scope.selectedAll;
        });
    };

    $scope.CalculateTotal = function () {
        $scope.Expense_Parent_Obj.Expense_Total_Amount = 0;

        angular.forEach($scope.Expense_Child_List, function (obj) {
            $scope.Expense_Parent_Obj.Expense_Total_Amount+= obj.Expense_Total_Amount;
        });
    };


    });