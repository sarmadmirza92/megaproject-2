﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Expense_Parent
    {
        [Key]
        public int Expense_Parent_id { get; set; }

        public string Id { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int Expense_Total_Amount { get; set; }

        //public string Expense_Description { get; set; }

        public bool Expense_Status { get; set; }

        public DateTime Expense_Date { get; set; }

    }
}