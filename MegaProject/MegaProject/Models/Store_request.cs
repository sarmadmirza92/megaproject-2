﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Store_request
    {
        [Key]
        public int Store_request_id { get; set; }
        public string Store_request_name { get; set; }
        public string Store_request_owner_name { get; set; }
        public string Store_request_type { get; set; }
        public string Store_request_discription { get; set; }
        public string Store_request_address { get; set; }
        public string Store_request_phone_no { get; set; }
        public DateTime Store_request_date { get; set; }
        public bool Store_request_status { get; set; }
    }
}