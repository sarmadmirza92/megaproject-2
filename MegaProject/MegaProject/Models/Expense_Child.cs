﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Expense_Child
    {
        [Key]
        public int Expense_Child_id { get; set; }

        public int Expense_Parent_id { get; set; }
        public virtual Expense_Parent Expense_Parent{ get; set; }

        public int Expense_Child_Amount { get; set; }

        public string Expense_Child_Description { get; set; }


    }
}