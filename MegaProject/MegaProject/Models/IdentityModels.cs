﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MegaProject.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public override string Email { get; set; }
        public bool Status { get; set; }
        public string phone_No { get; set; }
        public string Name { get; set; }

        public string SavedPassword { get; set; }

        public string UserType { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Store> Stores { get; set; }
        public DbSet<Count_model> Counts_model { get; set; }
        public DbSet<Contact> contacts { get; set; }
        public DbSet<Share> Shares { get; set; }
        public DbSet<Share_History> Share_Histories { get; set; }
        public DbSet<Share_Holder> Share_Holders { get; set; }
        public DbSet<Market> Markets { get; set; }
        public DbSet<Plaza> Plazas { get; set; }
        public DbSet<Expense_Parent> Expenses_Parent{ get; set; }
        public DbSet<Expense_Child> Expenses_Child { get; set; }
        public DbSet<Borrow> Borrows { get; set; }

        public System.Data.Entity.DbSet<MegaProject.Models.Store_request> Store_request { get; set; }
    }
}