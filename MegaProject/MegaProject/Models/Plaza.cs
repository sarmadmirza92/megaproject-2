﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Plaza
    {
        [Key]
        public int Plaza_id { get; set; }

        public int Market_id { get; set; }
        public virtual Market Market { get; set; }

        public string Plaza_Name { get; set; }

        public bool Plaza_Status { get; set; }
    }
}