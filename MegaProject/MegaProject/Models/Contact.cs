﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Contact
    {
        [Key]
        public int ContactId { get; set; }
        public string  contact_Name { get; set; }

        public string Contact_Email { get; set; }

        public string Contact_Subject { get; set; }

        public string Contact_Message { get; set; }



    }
}