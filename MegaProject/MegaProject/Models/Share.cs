﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Share
    {
    
        [Key]
        public int Share_id { get; set; }

        public decimal Share_Percentage { get; set; }

        //public string Share_Type { get; set; }

        public int Share_Amount { get; set; }

        public int  Share_Code { get; set; }

        public DateTime Share_Date { get; set; }

    }
}