﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Market
    {
        [Key]
        public int Market_id { get; set; }

        public string Market_Name { get; set; }

        public bool Market_Status { get; set; }

    }
}