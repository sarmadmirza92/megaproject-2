﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Count_model
    {
        [Key]
        public int count_id { get; set; }
        
        public string visiter_ip { get; set; }
        public int StoreId { get; set; }
        public virtual Store Store { get; set; }
        
        public string count_date { get; set; }
        public int count { get; set; }
    }
}