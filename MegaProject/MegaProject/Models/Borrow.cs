﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Borrow
    {
        [Key]
        public int Borrow_id { get; set; }

        public string Borrow_Description { get; set; }

        public int Borrow_Amount { get; set; }

        public bool Borrow_Status { get; set; }

    }
}