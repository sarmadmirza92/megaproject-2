﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Share_History
    {
        [Key]
        public int Share_History_id { get; set; }

        public int Share_id { get; set; }
        public virtual Share Share { get; set; }


        public int Share_History_Amount { get; set; }

        public DateTime Share_History_Date { get; set; }

   


    }
}