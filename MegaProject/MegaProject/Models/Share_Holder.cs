﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MegaProject.Models
{
    public class Share_Holder
    {
        [Key]
        public int Share_Holder_id { get; set; }

        public string Id { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int Share_History_id { get; set; }
        public virtual Share_History Share_History { get; set; }

        public string Share_Holder_Name{ get; set; }

        public string Share_Holder_UniqueId { get; set; }

        public string Share_Holder_Password { get; set; }

        public DateTime Share_Holder_Date { get; set; }


    }
}