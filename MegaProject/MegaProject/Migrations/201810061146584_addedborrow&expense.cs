namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedborrowexpense : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Borrows",
                c => new
                    {
                        Borrow_id = c.Int(nullable: false, identity: true),
                        Borrow_Description = c.String(),
                        Borrow_Amount = c.Int(nullable: false),
                        Borrow_Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Borrow_id);
            
            CreateTable(
                "dbo.Expenses",
                c => new
                    {
                        Expense_id = c.Int(nullable: false, identity: true),
                        Id = c.String(maxLength: 128),
                        Expense_Amount = c.Int(nullable: false),
                        Expense_Description = c.String(),
                        Expense_Status = c.Boolean(nullable: false),
                        Expense_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Expense_id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Expenses", "Id", "dbo.AspNetUsers");
            DropIndex("dbo.Expenses", new[] { "Id" });
            DropTable("dbo.Expenses");
            DropTable("dbo.Borrows");
        }
    }
}
