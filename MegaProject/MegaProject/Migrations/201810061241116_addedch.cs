namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedch : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Expenses", "Id", "dbo.AspNetUsers");
            DropIndex("dbo.Expenses", new[] { "Id" });
            CreateTable(
                "dbo.Expense_Child",
                c => new
                    {
                        Expense_Child_id = c.Int(nullable: false, identity: true),
                        Expense_Parent_id = c.Int(nullable: false),
                        Expense_Child_Amount = c.Int(nullable: false),
                        Expense_Child_Description = c.String(),
                    })
                .PrimaryKey(t => t.Expense_Child_id)
                .ForeignKey("dbo.Expense_Parent", t => t.Expense_Parent_id, cascadeDelete: true)
                .Index(t => t.Expense_Parent_id);
            
            CreateTable(
                "dbo.Expense_Parent",
                c => new
                    {
                        Expense_Parent_id = c.Int(nullable: false, identity: true),
                        Id = c.String(maxLength: 128),
                        Expense_Total_Amount = c.Int(nullable: false),
                        Expense_Status = c.Boolean(nullable: false),
                        Expense_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Expense_Parent_id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
            DropTable("dbo.Expenses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Expenses",
                c => new
                    {
                        Expense_id = c.Int(nullable: false, identity: true),
                        Id = c.String(maxLength: 128),
                        Expense_Amount = c.Int(nullable: false),
                        Expense_Description = c.String(),
                        Expense_Status = c.Boolean(nullable: false),
                        Expense_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Expense_id);
            
            DropForeignKey("dbo.Expense_Child", "Expense_Parent_id", "dbo.Expense_Parent");
            DropForeignKey("dbo.Expense_Parent", "Id", "dbo.AspNetUsers");
            DropIndex("dbo.Expense_Parent", new[] { "Id" });
            DropIndex("dbo.Expense_Child", new[] { "Expense_Parent_id" });
            DropTable("dbo.Expense_Parent");
            DropTable("dbo.Expense_Child");
            CreateIndex("dbo.Expenses", "Id");
            AddForeignKey("dbo.Expenses", "Id", "dbo.AspNetUsers", "Id");
        }
    }
}
