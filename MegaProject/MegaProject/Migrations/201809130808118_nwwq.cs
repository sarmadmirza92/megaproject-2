namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nwwq : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Count_model", "count_date", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Count_model", "count_date", c => c.DateTime(nullable: false));
        }
    }
}
