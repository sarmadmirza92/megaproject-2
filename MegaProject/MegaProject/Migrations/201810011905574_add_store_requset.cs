namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_store_requset : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Store_request", "Store_request_date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Store_request", "Store_request_status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Store_request", "Store_request_status");
            DropColumn("dbo.Store_request", "Store_request_date");
        }
    }
}
