// <auto-generated />
namespace MegaProject.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class usertableaddition : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(usertableaddition));
        
        string IMigrationMetadata.Id
        {
            get { return "201809301622102_user table addition"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
