namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Shares", "Share_Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Shares", "Share_Type", c => c.String());
        }
    }
}
