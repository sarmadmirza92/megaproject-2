namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newqqqq123 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "user_name", c => c.String());
            AddColumn("dbo.Stores", "password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stores", "password");
            DropColumn("dbo.Stores", "user_name");
        }
    }
}
