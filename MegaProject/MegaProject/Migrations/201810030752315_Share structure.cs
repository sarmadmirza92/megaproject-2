namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sharestructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Share_History",
                c => new
                    {
                        Share_History_id = c.Int(nullable: false, identity: true),
                        Share_id = c.Int(nullable: false),
                        Share_History_Amount = c.Int(nullable: false),
                        Share_History_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Share_History_id)
                .ForeignKey("dbo.Shares", t => t.Share_id, cascadeDelete: true)
                .Index(t => t.Share_id);
            
            CreateTable(
                "dbo.Shares",
                c => new
                    {
                        Share_id = c.Int(nullable: false, identity: true),
                        Share_Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Share_Type = c.String(),
                        Share_Amount = c.Int(nullable: false),
                        Share_Code = c.Int(nullable: false),
                        Share_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Share_id);
            
            CreateTable(
                "dbo.Share_Holder",
                c => new
                    {
                        Share_Holder_id = c.Int(nullable: false, identity: true),
                        Id = c.String(maxLength: 128),
                        Share_History_id = c.Int(nullable: false),
                        Share_Holder_Name = c.String(),
                        Share_Holder_UniqueId = c.String(),
                        Share_Holder_Password = c.String(),
                        Share_Holder_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Share_Holder_id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .ForeignKey("dbo.Share_History", t => t.Share_History_id, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.Share_History_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Share_Holder", "Share_History_id", "dbo.Share_History");
            DropForeignKey("dbo.Share_Holder", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Share_History", "Share_id", "dbo.Shares");
            DropIndex("dbo.Share_Holder", new[] { "Share_History_id" });
            DropIndex("dbo.Share_Holder", new[] { "Id" });
            DropIndex("dbo.Share_History", new[] { "Share_id" });
            DropTable("dbo.Share_Holder");
            DropTable("dbo.Shares");
            DropTable("dbo.Share_History");
        }
    }
}
