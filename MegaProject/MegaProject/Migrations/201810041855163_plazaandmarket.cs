namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class plazaandmarket : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Markets",
                c => new
                    {
                        Market_id = c.Int(nullable: false, identity: true),
                        Market_Name = c.String(),
                        Market_Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Market_id);
            
            CreateTable(
                "dbo.Plazas",
                c => new
                    {
                        Plaza_id = c.Int(nullable: false, identity: true),
                        Market_id = c.Int(nullable: false),
                        Plaza_Name = c.String(),
                        Plaza_Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Plaza_id)
                .ForeignKey("dbo.Markets", t => t.Market_id, cascadeDelete: true)
                .Index(t => t.Market_id);
            
            DropColumn("dbo.Shares", "Share_Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Shares", "Share_Type", c => c.String());
            DropForeignKey("dbo.Plazas", "Market_id", "dbo.Markets");
            DropIndex("dbo.Plazas", new[] { "Market_id" });
            DropTable("dbo.Plazas");
            DropTable("dbo.Markets");
        }
    }
}
