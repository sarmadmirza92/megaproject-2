namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stroe_discription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Stores", "Discription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Stores", "Discription");
        }
    }
}
