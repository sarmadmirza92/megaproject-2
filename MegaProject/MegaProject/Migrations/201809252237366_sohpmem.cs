namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sohpmem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Store_request",
                c => new
                    {
                        Store_request_id = c.Int(nullable: false, identity: true),
                        Store_request_name = c.String(),
                        Store_request_owner_name = c.String(),
                        Store_request_type = c.String(),
                        Store_request_discription = c.String(),
                        Store_request_address = c.String(),
                        Store_request_phone_no = c.String(),
                    })
                .PrimaryKey(t => t.Store_request_id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Store_request");
        }
    }
}
