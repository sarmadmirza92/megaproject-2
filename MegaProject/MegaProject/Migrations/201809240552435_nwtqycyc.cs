namespace MegaProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nwtqycyc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "contact_Name", c => c.String());
            AddColumn("dbo.Contacts", "Contact_Email", c => c.String());
            AddColumn("dbo.Contacts", "Contact_Subject", c => c.String());
            AddColumn("dbo.Contacts", "Contact_Message", c => c.String());
            DropColumn("dbo.Contacts", "Name");
            DropColumn("dbo.Contacts", "Email");
            DropColumn("dbo.Contacts", "Subject");
            DropColumn("dbo.Contacts", "Message");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "Message", c => c.String());
            AddColumn("dbo.Contacts", "Subject", c => c.String());
            AddColumn("dbo.Contacts", "Email", c => c.String());
            AddColumn("dbo.Contacts", "Name", c => c.String());
            DropColumn("dbo.Contacts", "Contact_Message");
            DropColumn("dbo.Contacts", "Contact_Subject");
            DropColumn("dbo.Contacts", "Contact_Email");
            DropColumn("dbo.Contacts", "contact_Name");
        }
    }
}
